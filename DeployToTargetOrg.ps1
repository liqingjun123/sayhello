#! /usr/bin/pwsh

# Copyright of PwC AC
param(
     [Parameter()]
     [string]$SF_SRC_DIR,
 
     [Parameter()]
     [string]$SF_PKG_NAME,
 
     [Parameter()]
     [string]$SF_TRG_ORG_ALIAS,
     
     [Parameter()]
     [string]$SF_API_VERSION,
 
     [Parameter()]
     [string]$SF_CHECK_ONLY,
 
     [Parameter()]
     [string]$SF_TEST_LEVEL,    
     
     [Parameter()]
     [string]$SF_TEST_CLASS_NAMES     
 )
try {
    Write-Host "Project Path: $SF_PKG_NAME"
    
    If ($SF_PKG_NAME -ne 'NUL') {
    	cd  $SF_PKG_NAME
    }

    $args = @()

    $alias = $SF_TRG_ORG_ALIAS -replace (' ', '')
    $apiVersion = $SF_API_VERSION -replace (' ', '')
    $isCheckOnly = $SF_CHECK_ONLY -replace (' ', '')
    $srcDir = $SF_SRC_DIR -replace (' ', '')
    $testLevel = $SF_TEST_LEVEL -replace (' ', '')
    $testClassNames = $SF_TEST_CLASS_NAMES -replace (' ', '')
    $ignoreWarning = 'true'

    If ($apiVersion -ne '') {
        $args += "--apiversion=$apiVersion"
    }

    If ($isCheckOnly -eq 'true') {
        $args += '--checkonly'
    }

    If ($testLevel -ne 'NUL') {
            $args += "--testlevel=$testLevel"
    }

    If ($testClassNames -ne 'NUL') {
            $args += "--runtests=$testClassNames"
    }

    If ($srcDir -eq '') {
        $args += '--sourcepath="./force-app"'
    } else {
        $args += "--sourcepath=""./$srcDir"""
    }

   if ($ignoreWarning -ne 'false'){
        $args += "--ignorewarnings"
   }


    $args += "--targetusername=$alias"
    $args += "--verbose"
    $args += "--wait=120"    

    Write-Host "DX arguments: $args"

    & 'sfdx' 'force:source:deploy' $args
    #sfdx force:source:deploy -c -g -p force-app
    
    Write-Host "Successfully"

} catch {
    Write-Host "Failed"
    $msg = $_.Exception.Message
    Write-Host "##vso[task.logissue type=error] $msg"
    exit 1
}
