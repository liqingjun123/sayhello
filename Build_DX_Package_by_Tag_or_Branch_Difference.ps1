#! /usr/bin/pwsh

# Copyright of PwC AC
param(
     [Parameter()]
     [string]$SF_SRC_DIR,
 
     [Parameter()]
     [string]$SF_TAG_NAME,
 
     [Parameter()]
     [string]$BUILD_SOURCESDIRECTORY,
     
     [Parameter()]
     [string]$BUILD_STAGINGDIRECTORY,
 
     [Parameter()]
     [string]$SF_DEPLOY_PROFILE
 )
#[Inline Powershell] Build DX Packfage by Tag or Branch Difference

#Laod congfiure file from specific place
#$configPath = "C:\Users\admin\Desktop\Build.json"
#$configPath = "configFilePath"
#$objConfig = (Get-Content $configPath)|ConvertFrom-Json

Write-Host 'Starting: Build DX Package by Tag or Branch Difference'
Write-Host $SF_TAG_NAME

git config diff.renames 0
git config advice.detachedHead false
git config core.quotePath false
#${env:GIT_REDIRECT_STDERR} = '2>&1'
<#
$SF_SRC_DIR = $objConfig.SF_SRC_DIR
$SF_TAG_NAME = $objConfig.SF_TAG_NAME
$BUILD_SOURCESDIRECTORY = $objConfig.BUILD_SOURCESDIRECTORY
$BUILD_STAGINGDIRECTORY = $objConfig.BUILD_STAGINGDIRECTORY
$SF_DEPLOY_PROFILE = $objConfig.SF_DEPLOY_PROFILE
$SF_CREATE_ROLLBACK = $objConfig.SF_CREATE_ROLLBACK
#>

# @author: Liquad Li
# @description: Get all files/folders for the package by changed files.
function Get-PackageFiles {
    param (        
        $allChanges
    )

    process {

        #-meta.xml file also need copy for postfix
        $MetaMapExt2Folder=@{
            cls                      = @{type="ApexClass";folder="classes"};
            page                     = @{type="ApexPage";folder="pages"};
            component                = @{type="ApexComponent";folder="components"};
            trigger                  = @{type="ApexTrigger";folder="triggers"};

            email                    = @{type="EmailTemplate";folder="email"};
         
            wdf                      = @{type="WaveDataflow";folder="wave"};
            wdash                    = @{type="WaveDashboard";folder="wave"};
            wlens                    = @{type="WaveLens";folder="wave"};
        }

        #whole folder need copy
        $FolderMap=@{
            lwc                      = @{type="LightningComponentBundle"};
            aura                     = @{type="AuraDefinitionBundle"};
            objectTranslations       = @{type="CustomObjectTranslation"};
            staticresources          = @{type="StaticResource";postfix="resource";needcopymeta="true"};
        }

        #-meta.xml file also need copy for folder
        $FolderNeedCopyMetaFile = @{
            siteDotComSites          = @{type="SiteDotCom";postfix="site"};
        }
        
        $changeset = @()
        Write-Host "allChanges.Count:" $allChanges.Count

        foreach($path in $allChanges) {

            if($path.Contains("$SF_SRC_DIR/main/default/")){
                $path = [System.Text.Encoding]::UTF8.GetString([System.Text.Encoding]::Default.GetBytes($path))

                $filepath = $path -replace "$SF_SRC_DIR/main/default/",""
Write-Host $filepath
                if ($filepath -inotmatch "/") {
                    Write-Host "Ignored files: $path"
                    $path | Out-File -Append "$BUILD_STAGINGDIRECTORY/filesIgnored.txt"
                    continue
                }

                #e.g. report/<reportfolder>/xxxxxx.yyy.report-meta.xml
                #e.g. report
                $folderName = ($filepath -split "/")[0]
                #Write-Host $path
Write-Host $folderName

                #e.g. <reportfolder>/xxxxxx.yyy.report-meta.xml
                $fileName = $filepath.SubString($folderName.Length+1)

                #e.g. <reportfolder>/xxxxxx.yyy.report
                if ($fileName.EndsWith("-meta.xml")){
                    $fileName = $fileName.SubString(0, $fileName.Length - 9)
                }
Write-Host $fileName
                #e.g. xxxx.yyy.md
                $fileParts = $fileName.Split(".")
Write-Host $fileParts
                if ($fileParts.Count -eq 1){
                    Write-Host "Ignored files: $path"
                    $path | Out-File -Append "$BUILD_STAGINGDIRECTORY/filesIgnored.txt"
                    continue
                }
                $postfix = $fileParts[$fileParts.Length-1]
                
                #Event/validationRules
                #e.g. <reportfolder>/xxxxxx.yyy
                $member = $fileName.SubString(0, $fileName.Length - $postfix.Length -1)
Write-Host $member              
                if($FolderMap.ContainsKey($folderName)){
                    $metaType = $FolderMap[$folderName].type
                    
                    if ($member -imatch "/") {
                        $member = $fileName.Split("/")[0]
                    }
                    
                    $needCopy = "$folderName/$member"

                    if (-not($changeset.Contains($needCopy))){
                        $changeset += $needCopy
                        Write-Host $needCopy
                    }

                    if ($FolderMap[$folderName].needcopymeta){
                        $metafilePostFix = $FolderMap[$folderName].postfix
                        $needCopy = "$folderName/$member.$metafilePostFix-meta.xml"

                        if (-not($changeset.Contains($needCopy))){
                            $changeset += $needCopy
                            Write-Host $needCopy
                        }                        
                    }
                   
                }
                elseif(($MetaMapExt2Folder.ContainsKey($postfix)) -or ($FolderNeedCopyMetaFile.ContainsKey($folderName))){

                    $needCopy = $filepath

                    if (-not($changeset.Contains($needCopy))){
                        $changeset += $needCopy
                        Write-Host $needCopy
                    }

                    if ($filepath.EndsWith("-meta.xml")){
                        $needCopy = $filepath.SubString(0, $filepath.Length-9)
                    }else {
                        $needCopy = "$filepath-meta.xml"
                    }

                    if (-not($changeset.Contains($needCopy))){
                        $changeset += $needCopy
                        Write-Host $needCopy
                    }
                }
                else{
                    $needCopy = $filepath
                    if (-not($changeset.Contains($needCopy))){
                        $changeset += $needCopy
                        Write-Host $needCopy
                    }
                }
            }
        }

        $changeset
    }
}

# @author: Liquad Li
# @description: Build the package by copying files to destination.
function Copy-Files {
    param (
        $changeset,
        $srcParent,
        $destParent
        )
    process {
        foreach($item in $changeset){
            $srcPath = Join-Path -Path $srcParent -ChildPath $item
            Write-Host "Processing $srcPath"

            $destFile = Join-Path -Path $destParent -ChildPath $item
            if (Test-Path -Path $srcPath -PathType Container) {
                           
                Copy-Item -Path $srcPath -Destination $destFile -Recurse -Force

            }elseif (Test-Path -Path $srcPath){

                $destPath = Split-Path -Path $destFile

                if (-not(Test-Path -Path $destPath -PathType Container)){
                    mkdir -p $destPath | Out-Null
                }

                Copy-Item -Path $srcPath -Destination $destFile -Force
            }elseif (($srcPath.IndexOf("\staticresources\") -gt 1) -and (Test-Path -Path ($srcPath + ".*") -Exclude "*.resource-meta.xml")){
                # single file static res
                $srcPath = Get-Item -Path ($srcPath + ".*") -Exclude "*.resource-meta.xml"
                $fileName = Split-Path -Path $srcPath -Leaf
                $destPath = Split-Path -Path $destFile
                $destFile = Join-Path -Path $destPath -ChildPath $fileName

                if (-not(Test-Path -Path $destPath -PathType Container)){
                    mkdir -p $destPath | Out-Null
                }

                Copy-Item -Path $srcPath -Destination $destFile -Force
            }else{
                Write-Host "Missing files: $srcPath"
                $srcPath | Out-File -Append "$BUILD_STAGINGDIRECTORY/missingFiles.txt"
            }
        }
    }
}

# @author: Liquad Li
# @description: Build the package by copying files to destination.
function mergeCustomLabel {
    param (
        $customLabelPath = "$SF_SRC_DIR/main/default/labels"
        )
    process {

            $postfix = ".labels-meta.xml"
            $filename = "CustomLabels" + $postfix
            $nodename = "CustomLabels.labels"
            $folder = "labels"

            $wholeFilePath = "$customLabelPath/$filename"

         if (-not(Test-Path -Path $customLabelPath)){
            return
         }

         $allFiles = Get-ChildItem -Path $customLabelPath
         if ($allFiles.Count -eq 0) {
            return
         }elseif ($allFiles.Count -eq 1){
            Move-Item -Path $allFiles[0].PSPath -Destination $wholeFilePath -Force
            return
         }

         $allNodes = [xml](Get-Content $allFiles[0].PSPath -Encoding UTF8)
         Remove-Item -Path $allFiles[0].PSPath

         for($index=1;$index -lt $allFiles.Count; $index++) {
            $filepath = $allFiles[$index].PSPath

            if ($filepath -eq $wholeFilePath){
                continue
            }

            #get xml data via filename
            [xml]$xmldata = (Get-Content $filepath -Encoding UTF8)
            $nodes = Invoke-Expression "`$xmldata.$nodename"

            foreach($subj in $nodes) {
                $newNode = $allNodes.ImportNode($subj, $true)
                $allNodes.ChildNodes[1].AppendChild($newNode) | Out-Null
            }

            Remove-Item -Path $filepath
         }

         if(-Not (Test-Path -Path $customLabelPath)){
            md $customLabelPath
         }

         $allNodes.Save($wholeFilePath)

     }
}

function merge-Translations {
    param (
        $typePath = "$SF_SRC_DIR/main/default/translations",
        $language
        )
    process {
         $folder = "translations"
         $postfix = ".translation-meta.xml"
         $filename = $language + $postfix
         $wholeFilePath = "$typePath/$filename"

         if (-not(Test-Path -Path $typePath)){
            return
         }
         Write-Host "Merging $typePath"
         $allFiles = Get-ChildItem -Path "$typePath/*.$filename"
         $fileCount = $allFiles.Count
         Write-Host "Found files: $fileCount"
         if ($allFiles.Count -eq 0) {
            Write-Host "No files to be merged"
            return
         }

         
        $header = '<?xml version="1.0" encoding="utf-8"?>' + "`r`n" + '<Translations xmlns="http://soap.sforce.com/2006/04/metadata">'
        $footer = "`r`n</Translations>"
        $intent = "    "
        $xmlns = ' xmlns="http://soap.sforce.com/2006/04/metadata"'

        $labelBodies = ""
        for($index=0;$index -lt $allFiles.Count; $index++) {
            $filepath = $allFiles[$index].PSPath

            if ($filepath -eq $wholeFilePath){
                continue
            }
            Write-Host "Processing $filepath"

            #get xml data via filename
            [xml]$xmldata = (Get-Content $filepath -Encoding UTF8)
            $nodes = $xmldata.Translations.ChildNodes

            foreach($subj in $nodes) {
                $labelBodies += "`r`n" + $intent + $subj.OuterXml.Replace($xmlns, '')
            }

            Remove-Item -Path $filepath
         }

         if(-Not (Test-Path -Path $typePath)){
            md $typePath
         }

         $wholeContent = $header + $labelBodies + $footer
         Set-Content -Path $wholeFilePath -Value $wholeContent -Encoding UTF8
     }
}

function merge-Profile {
    param (
        $profileSubPath = "$SF_SRC_DIR/main/default/profiles/<profileName>",
        $removeSrc = $false
        )
    process {
            $folder = "profiles"
            $rootNode = "Profile"
            $postfix = ".profile-meta.xml"
            $profileName = Split-Path -Path $profileSubPath -Leaf
            $typePath = Split-Path -Path $profileSubPath
            $filename = $profileName + $postfix

            $wholeFilePath = Join-Path -Path $typePath -ChildPath $filename

         if (-not(Test-Path -Path $profileSubPath)){
            return
         }

        $settings = New-Object System.Xml.XmlWriterSettings
        $settings.IndentChars = "    "
        $settings.Indent = $true
        $settings.Encoding = New-Object System.Text.UTF8Encoding($false)
        Write-Host "Merging $profileSubPath"
         $allFiles = Get-ChildItem -Path "$profileSubPath/*$postfix"
         $fileCount = $allFiles.Count
         Write-Host "Found files: $fileCount"
         if ($allFiles.Count -eq 0) {
            Write-Host "No files to be merged"
            Remove-Item -Path $profileSubPath -Force
            return
         }elseif ($allFiles.Count -eq 1){
            Move-Item -Path $allFiles[0].PSPath -Destination $wholeFilePath -Force
            Remove-Item -Path $profileSubPath -Force
            return
         }

         $allNodes = [xml](Get-Content $allFiles[0].PSPath -Encoding UTF8)

         for($index=1;$index -lt $allFiles.Count; $index++) {
            $filepath = $allFiles[$index].PSPath

            if ($filepath -eq $wholeFilePath){
                continue
            }

            Write-Host "Processing $filepath"

            #get xml data via filename
            [xml]$xmldata = (Get-Content $filepath -Encoding UTF8)
            $nodes = $xmldata.$rootNode.ChildNodes

            foreach($subj in $nodes) {
                $newNode = $allNodes.ImportNode($subj, $true)
                $allNodes.ChildNodes[1].AppendChild($newNode) | Out-Null
            }
         }

         if(-Not (Test-Path -Path $typePath)){
            md $typePath
         }

         $allNodes.Save([System.Xml.XmlWriter]::Create($wholeFilePath, $settings))

         if ($removeSrc) {
            Remove-Item -Path $profileSubPath -Force -Recurse
         }
     }
}

function merge-ProfileInFolder {
    param ($Path)
    process {
        if (-not(Test-Path -Path $Path -PathType Container)){
            return
        }
        $allFiles = Get-ChildItem -Path $Path -Directory
        $allFilesCount = $allFiles.Count
        Write-Host "All profiles count: $allFilesCount"

        foreach ($oneFile in $allFiles) {
            merge-Profile -profileSubPath $oneFile.FullName -removeSrc $true
        }
    }
}

function NothingToDeploy(){
    Write-Host "Nothing need to deployed"
    $nodeploy = "true"
    Write-Host "##vso[task.setvariable variable=env:SF_NO_DEPLOY]$nodeploy"
}

function CheckFatalError {
    param (        
        $allChanges
    )
    process {
        foreach ($one in $allChanges) {
          if ($one.StartsWith("fatal:")){
            Write-Host $allChanges
            throw "Fatal errors"
          }
          
          return
        }
    }
}


if(-not(Test-path -Path "$BUILD_STAGINGDIRECTORY/package")){
    New-Item -ItemType Directory -Force "$BUILD_STAGINGDIRECTORY/package" | Out-Null
}

$xmlNameDestructive = "destructiveChanges"
$xmlNamePackage = "package"
$nothingToDeploy = "Nothing need to deployed"

#Package
Write-Host "`r`nStarting building package"
$allChanges = &git diff --name-only $SF_TAG_NAME --diff-filter=d
                                  
if (-not($allChanges)){
    NothingToDeploy
    return
}

CheckFatalError($allChanges)

if ($allChanges.Count -gt 0){
    $changeset = Get-PackageFiles -allChanges $allChanges 
    if (-not($changeset)){
        NothingToDeploy
        return
    }

    if ($changeset.Count -gt 0) {
        Copy-Files -changeset $changeset -srcParent $BUILD_SOURCESDIRECTORY/$SF_SRC_DIR/main/default -destParent $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default
        mergeCustomLabel -customLabelPath $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default/labels
        merge-Translations -language "es_MX" -typePath $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default/translations
        merge-Translations -language "en_US" -typePath $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default/translations
        merge-Translations -language "pt_BR" -typePath $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default/translations
        merge-ProfileInFolder -Path $BUILD_STAGINGDIRECTORY/package/$SF_SRC_DIR/main/default/profiles
        Copy-Item -Path $BUILD_SOURCESDIRECTORY/sfdx-project.json -Destination $BUILD_STAGINGDIRECTORY/package/sfdx-project.json -Force
        #Copy-Item -Path $BUILD_SOURCESDIRECTORY/.forceignore -Destination $BUILD_STAGINGDIRECTORY/package/.forceignore -Force
        if ($SF_DEPLOY_PROFILE -eq "false"){
            add-content $BUILD_STAGINGDIRECTORY/package/.forceignore "`r`n**/profiles/**"
        }
        Set-Content -Value $allChanges -Path "$BUILD_STAGINGDIRECTORY/package/$xmlNamePackage.txt"
    } else {
       NothingToDeploy
       return
    }
}

#destructiveChanges
Write-Host "`r`nStarting building destructiveChanges"
$allChanges = &git diff --name-only $SF_TAG_NAME --diff-filter=D
if ($allChanges){
    CheckFatalError($allChanges)
    if ($allChanges.Count -gt 0){
            Set-Content -Value $allChanges -Path "$BUILD_STAGINGDIRECTORY/package/$xmlNameDestructive.txt"
    }
}

Write-Host 'Finishing: [Inline Powershell] Build DX Package by Tag or Branch Difference'